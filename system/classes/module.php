<?php defined('ABSPATH') or die ('Not allowed!');

class Module
{
    protected static $db;

    public static function __callStatic($module, $param) {
        $modClass = ucfirst($module);
        if (isset(App::$mods[$module]) && is_subclass_of($modClass, 'Module')) {
            if (!App::enabledMod($module)) {
                App::error('Module "'.$module.'" not found or it\'s disabled.');
            } else {
                $instance =& App::$mods[$module]['instance'];
                return $instance;
            }
        }
    }

    final protected function initialize($name, array $configs = array()) {
        static::$db =& Db::instance();

        App::$menu->add($name, $configs);
    }

    // protected static function api()
    // {}
}

// EOF module.php
