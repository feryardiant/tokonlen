<?php defined('ABSPATH') or die ('Not allowed!');

class Error
{
    public static function errHandler($errno, $message, $file, $line, $context) {
        $die = false;
        switch ($errno) {
            case E_USER_ERROR:
                $type = 'error';
                $die = true;
            break;
            case E_USER_WARNING:
            case E_WARNING:
            case @E_RECOVERABLE_ERROR:
                $type = 'warning';
            break;
            case E_USER_NOTICE:
            case E_NOTICE:
            case @E_STRICT:
                $type = 'notice';
            break;
            default:
                $type = '';
                $die = true;
            break;
        }

        $text = $message;
        $file = str_replace(array(ABSPATH, '/'), array('', DS), $file);
        $message = '<strong>'.$text.'</strong>';

        if (App::conf('debug')) {
            $message .= ' in <code>'.$file.' ('.$line.')</code>';
        }

        App::alert($message, $type, $die);
    }

    public static function excHandler($exc) {
        App::alert($exc);
    }
}

// EOF error.php
