<?php defined('ABSPATH') or die ('Not allowed!');

class App
{
    private static
        // Instansi
        $instance,
        // URI Segments
        $uri,
        // Buffer Level
        $buffLevel,
        // Message Wrapper
        $messages = array();

    /**
     * Menu Instance
     *
     * @var  array
     */
    public static
        // Menu Instance
        $menu,
        // Modules
        $mods = array(),
        // Konfigurasi
        $conf = array(
            'basename'  => 'aplikasi',
            'baseurl'   => '',
            'modpath'   => 'modules',
            'template'  => 'base',
            'basefile'  => 'index',
            'debug'     => false,
            'session'   => false,
            'fmtdate'   => 'd-m-Y',
            'db.limit'  => 5,
            'app.title' => 'Mentehan',
            'app.desc'  => 'Sekedar Aplikasi PHP Sederhana',
        );

    /**
     * Class Constructor
     *
     * @param  array  $configs  Konfigurasi
     */
    function __construct(array $configs = array()) {
        $this->initialize($configs);

        self::$buffLevel = ob_get_level();
        self::$uri = explode('/', self::getUri());
        self::$menu = new Menu();
        self::$instance =& $this;
    }

    /**
     * Method untuk mendapatkan instansi dari class
     *
     * @return  resource
     */
    public static function &instance() {
        return self::$instance;
    }

    /**
     * Method untuk menginisialisasi konfigurasi
     *
     * @param   array   $configs  Konfigurasi
     * @return  void
     */
    public function initialize(array $configs = array()) {
        // Menerapkan setiap konfigurasi dari $configs
        foreach (array_merge(static::$conf, $configs) as $confKey => $confValue) {
            static::conf($confKey, $confValue);
        }

        $this->debug(static::$conf['debug']);
        // Default konfigurasi $baseurl
        if (static::$conf['baseurl'] == '') {
            static::$conf['baseurl'] = '//'.static::request('host').'/';
        }

        // Memulai sesi aplikasi
        if (!empty(static::$conf['basename']) && static::$conf['session'] !== false) {
            session_name(static::$conf['basename']);
            session_start();
        }

        static::$conf['modpath'] = rtrim(static::$conf['modpath'], '/').DS;
    }

    /**
     * Method untuk menerapkan atau mendapatkan konfigurasi.  Kosongkan $value
     * untuk mendapatkan nilai dari $key
     *
     * @param   string  $key    Key atau Nama konfigurasi
     * @param   mixed   $value  Nilai konfigurasi bisa berupa string atau array
     * @return  mixed
     */
    public static function conf($key, $value = null) {
        if ($value !== null) {
            if (is_array($value)) {
                foreach ($value as $vKey => $vValue) {
                    static::conf($key.'.'.$vKey, $vValue);
                }
            } else {
                static::$conf[$key] = $value;
            }
        } else {
            if (isset(static::$conf[$key])) {
                return static::$conf[$key];
            }
        }
    }

    // -------------------------------------------------------------------------

    /**
     * Method untuk memuat semua module
     *
     * @return  array
     */
    protected function loadMods() {
        $modules = array();

        foreach (func_get_args() as $path) {
            $dir = dir($path);
            while (false !== ($module = $dir->read())) {
                $modPath = $path.$module.DS;
                $modFile = $modPath.'function'.EXT;

                if (!in_array($module, array('.', '..')) && file_exists($modFile)) {
                    require_once $modFile;
                    $modules[$module] = array(
                        'path' => $modPath,
                        'enabled' => true,
                    );

                    $modClass = ucfirst($module);
                    if (class_exists($modClass)) {
                        $modules[$module]['instance'] = new $modClass();
                    }
                }
            }

            $dir->close();
        }

        self::$mods = $modules;
    }

    /**
     * Method untuk mendapatkan daftar module
     *
     * @param   bool   $all  Opsi untuk menampilkan semua, ubah ke 'false' untuk
     *                       menampilkan module yang 'enabled' saja.
     * @return  array
     */
    public static function getMods($all = true) {
        $mods = array();
        if ($all === true) {
            $mods = array_keys(self::$mods);
        } else {
            foreach (self::$mods as $name => $conf) {
                if ($conf['enabled'] === true) {
                    $mods[] = $name;
                }
            }
        }

        return $mods;
    }

    /**
     * Method untuk memastikah bahwa module $name tersebut ada
     *
     * @param   string  $name  Module name
     * @return  bool
     */
    public static function hasMod($name) {
        return isset(self::$mods[$name]);
    }

    /**
     * Method untuk memastikah bahwa module $name tersebut ada
     *
     * @param   string  $name  Module name
     * @return  bool
     */
    public static function enabledMod($name) {
        return isset(self::$mods[$name]) && self::$mods[$name]['enabled'] === true;
    }

    /**
     * Method untuk mengaktifkan module $name
     *
     * @param   string  $name  Module name
     * @return  void
     */
    public static function enableMod($name) {
        if (self::hasMod($name)) {
            self::$mods[$name]['enabled'] = true;
        }
    }

    /**
     * Method untuk menonaktifkan module $name
     *
     * @param   string  $name  Module name
     * @return  void
     */
    public static function disableMod($name) {
        if (self::hasMod($name) && self::$mods[$name]['enabled'] == true) {
            self::$mods[$name]['enabled'] = false;
        }
    }

    /**
     * Method untuk mendapatkan 'path' module $name
     *
     * @param   string  $name  Module name
     * @return  string
     */
    public static function modPath($name) {
        if (self::hasMod($name)) {
            return self::$mods[$name]['path'];
        }
    }

    // -------------------------------------------------------------------------

    /**
     * Method untuk memulai aplikasi
     *
     * @return  string
     */
    public function start() {
        self::$menu->add('home', array(
            'title' => 'Beranda',
            'order' => 1,
        ));

        $modpath = self::conf('modpath');
        $this->loadMods(ABSPATH.$modpath);

        $module = self::uriSegment(1);
        if (($requestUri = self::getUri()) && $requestUri != 'home') {
            $basefile = self::conf('basefile');
            $mainpath = ABSPATH.$modpath.$requestUri;
            $filepath = (is_dir($mainpath) ? $mainpath.DS.$basefile : $mainpath);
            if (!file_exists($filepath.EXT) && ($requestMod = self::modPath($module))) {
                $filepath = $requestMod.DS.$basefile;
            }

            if (IS_AJAX && $module !== '') {
                $module = ucfirst($module);
                if (method_exists($module, 'api')) {
                    call_user_func_array(array($module, 'api'), array());
                }
            } else {
                $this->render($filepath);
            }
        } else {
            if ($module == '') redirect('home');
            $this->render(templateDir('index'));
        }
    }

    /**
     * Method untuk memuat file
     *
     * @param   string  $filepath  Lokasi file yang akan dimuat
     * @return  string
     */
    public function render($filepath, $status = 200, array $data = array()) {
        if (pathinfo($filepath, PATHINFO_EXTENSION) == '') {
            $filepath = $filepath.EXT;
        }

        $filepath = str_replace('//', '/', $filepath);
        $filepath = str_replace('/', DS, $filepath);

        if (!file_exists($filepath)) {
            $status = 404;
            $heading = 'Oops! Error bro.';
            $message = 'Tidak dapat memuat halaman \''.pathinfo($filepath, PATHINFO_FILENAME).
                       '\' atau halaman tesebut tidak ada dalam server kami.';
            $filepath = templateDir('error'.EXT);
        }

        $this->header($status);
        $page = get('p') ?: 'data';

        if (!empty($data)) {
            extract($data);
        }

        ob_start();
        require $filepath;
        unset($filepath);
        $contents = ob_get_clean();
        if (ob_get_level() > self::$buffLevel + 1) {
            ob_end_flush();
        }

        echo $contents;
    }

    public static function show404() {
        $self =& self::instance();
        $self->render('', 404);
    }

    /**
     * Method untuk menampilkan atau menyembunyikan Error System
     *
     * @param   bool    $enabled  True untuk menampilkan dan False untuk menyembunyikan
     * @return  void
     */
    public function debug($enabled = false) {
        self::conf('debug', $enabled);

        if ($enabled === true) {
            error_reporting(E_ALL);
            ini_set("display_errors", 1);
            ini_set("html_errors", 1);
        } else {
            error_reporting(0);
            ini_set("display_errors", 0);
        }
    }

    /**
     * Method untuk mendapatkan segmentasi URL
     *
     * @param   int     $num  Segment Url
     * @return  string
     */
    public static function uriSegment($num) {
        $num -= 1;
        if (isset(self::$uri[$num])) {
            return self::$uri[$num];
        }
    }

    // -------------------------------------------------------------------------

    /**
     * Method untuk menerapkan dan mendapatkan Session Aplikasi
     *
     * @param   string  $key    Session Key
     * @param   string  $value  Session Value (kosongkan untuk mendapatkan nilai
     *                          dari $key, dan isi jika ingin mengubah nilai $Key)
     * @return  mixed
     */
    public static function session($key, $value = false) {
        $sessname = static::$conf['basename'];

        // Jika $value bernilai false
        if ($value === false) {
            // Jika $key bertipe array
            if (is_array($key)) {
                // Menerapkan masing2 isi dari array kedalam session
                foreach ($key as $name => $val) {
                    self::session($name, $val);
                }
            } else {
                // Sebaliknya, akan menampilkan isi dari session $key (jika ada)
                if (isset($_SESSION[$sessname][$key])) {
                    return $_SESSION[$sessname][$key];
                }
                return false;
            }
        } else {
            // Sebaliknya, akan mengubah nilai dari $key ke $value yg baru
            $_SESSION[$sessname][$key] = $value;
        }
    }

    /**
     * Menghapus Semua Session
     *
     * @return  void
     */
    public static function clearSession($key) {
        $sessname = static::$conf['basename'];
        if (isset($_SESSION[$sessname][$key])) {
            unset($_SESSION[$sessname][$key]);
        }
    }

    /**
     * Menghapus Semua Session
     *
     * @return  void
     */
    public static function dropSession() {
        $sessname = static::$conf['basename'];
        unset($_SESSION[$sessname]);
        session_destroy();
    }

    public static function input($key, $method) {
        $method = '_'.strtoupper($method);
        if (isset($$method[$key])) {
            return escapeString($$method[$key]);
        }

        return '';
    }

    public function header($code = 200, $type = '') {
        $http = array(
            //Successful 2xx
            200 => 'OK',
            //Redirection 3xx
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            304 => 'Not Modified',
            //Client Error 4xx
            400 => 'Bad Request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            //Server Error 5xx
            500 => 'Internal Server Error',
        );

        $type || $type = 'text/html';
        if (isset($http[$code])) {
            if (IS_AJAX) {
                $type = 'application/json';
            }

            header('Content-type: '.$type.'; charset=utf-8', true, $code);
        }
    }

    public static function request($key, $value = '') {
        $headers = array(
            'useragent' => 'HTTP_USER_AGENT',
            'encoding' => 'HTTP_ACCEPT_ENCODING',
            'accept' => 'HTTP_ACCEPT',
            'method' => 'REQUEST_METHOD',
            'script' => 'SCRIPT_NAME',
            'query' => 'QUERY_STRING',
            'host' => 'HTTP_HOST',
            'path' => 'PATH_INFO',
            'uri' => 'REQUEST_URI',
        );

        if (!isset($headers[$key])) return;
        $header = $headers[$key];

        if (isset($_SERVER[$header])) {
            $request = strtolower($_SERVER[$header]);
            if (in_array($key, array('accept', 'encoding'))) {
                $request = explode(';', $request);
                $request = explode(',', $request[0]);
                $request = array_map('trim', $request);

                return $value != '' ? in_array($value, $request) : $request;
            }

            return $value != '' ? strtolower($value) == $request : $request;
        }
    }

    /**
     * Mendapatkan url cantik untuk aplikasi
     *
     * @param   bool    $prefix_slash  Apakah output url ingin diawali dengan '/' diawal.
     * @return  string
     */
    public static function getUri($prefix_slash = false) {
        if ($path = static::request('path')) {
            $uri = $path;
        } elseif ($path = static::request('uri')) {
            $uri = $path;
            $script = static::request('script');

            if (strpos($uri, $script) === 0) {
                $uri = substr($uri, strlen($script));
            } elseif (strpos($uri, dirname($script)) === 0) {
                $uri = substr($uri, strlen(dirname($script)));
            }

            if (strncmp($uri, '?/', 2) === 0) {
                $uri = substr($uri, 2);
            }

            $parts = preg_split('#\?#i', $uri, 2);
            $uri = $parts[0];

            if (isset($parts[1])) {
                $_SERVER['QUERY_STRING'] = $parts[1];
                parse_str($_SERVER['QUERY_STRING'], $_GET);
            } else {
                $_SERVER['QUERY_STRING'] = '';
                $_GET = array();
            }

            $uri = parse_url($uri, PHP_URL_PATH);
        } else {
            // Couldn't determine the URI, so just return false
            return false;
        }

        // Do some final cleaning of the URI and return it
        return ($prefix_slash ? '/' : '').str_replace(array('//', '../'), '/', trim($uri, '/'));
    }

    static function error($message) {
        throw new Exception($message);
    }

    static function alert($message, $type = '', $die = false) {
        $types = array(
            'warning' => 'Awas! bro',
            'error'   => 'Oops! Error bro',
            'notice'  => 'Info! bro',
            'success' => 'Success'
        );

        if ($type == '' || !in_array($type, array_keys($types))) {
            $type = 'warning';
        }

        if ($message instanceof Exception) {
            $type = 'error';
            $die = true;
            $message = $message->getMessage();
        }

        $message = '<strong>' . $types[$type] . ':</strong><br>' . $message;

        if (PHP_SAPI == 'cli') {
            $message = str_replace(
                array('<strong>', '</strong>', '<code>', '</code>', '<br>'),
                array('', '', '"', '"', PHP_EOL),
                $message
            );
        }

        $message = PHP_SAPI != 'cli' ? '<span class="alert '.$type.'">'.$message.'</span>' : PHP_EOL.$message.PHP_EOL;

        if ($die) {
            $app =& self::instance();
            $app->header(500);

            die($message);
        } else {
            echo $message;
        }
    }
}

// EOF app.php
