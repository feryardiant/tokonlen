<?php defined('ABSPATH') or die ('Not allowed!');

class Pdf extends Fpdf\Fpdf
{
    protected
        $docTitle, $docCop, $docDate,
        $conf = array(
            'fontFamily'  => 'Times',
            'fontStyle'   => '',
            'fontSize'    => 10,
            'tableBorder' => 1,
        );

    public function __construct($orientation = '', $unit = '', $size = '') {
        $orientation || $orientation = 'P';
        $unit        || $unit        = 'mm';
        $size        || $size        = 'A4';

        parent::__construct($orientation, $unit, $size);
        $this->setMargins(10, 10);
        $this->setCreator(App::conf('app.title'));
        $this->setAuthor(User::current('nama'));
    }

    public function docTitle($docTitle) {
        $this->docTitle = $docTitle;
        $this->docDate = date('d M Y, H');
        $this->setTitle($docTitle.' - '.$this->docDate);
    }

    public function docCop($docTitle) {
        $this->docCop = array(
            App::conf('dinas.kab'),
            App::conf('dinas.kec'),
            App::conf('dinas.desa'),
            'Alamat: '.App::conf('dinas.alamat'),
        );

        return $this->docTitle($docTitle);
    }

    public function header() {
        if ($this->docCop) {
            $ln = 1;
            $tln = count($this->docCop);
            $this->image('storage/logo.png', 10, 8, 18);
            $this->setFont($this->conf['fontFamily'], 'B', 10);

            foreach ($this->docCop as $cop) {
                $border = $ln == $tln ? 'B' : 0;
                $height = $ln == $tln ? 6 : 5;
                $this->cell(0, $height, $cop, $border, 0, 'C');
                $this->ln();
                $ln++;
            }

            $this->docCop = array();
            $this->ln();
        }

        $this->setFont($this->conf['fontFamily'], 'B', 14);
        $this->cell(0, 15, trim($this->docTitle), 0, 0, 'C');
        $this->ln();
    }

    public function setData(Db $resource, array $column) {
        $i = 0;
        $widths = $data = array();

        foreach ($column as $field => $header) {
            $widths[$field] = $this->getStringWidth($header);
        }

        if ($resource->count() > $i) {
            foreach ($resource->result() as $row) {
                foreach ($fields as $field) {
                    $_val = isset($row->$field) ? $row->$field : '-';
                    $_fWidth = $this->getStringWidth($_val);
                    $widths[$field] = ($widths[$field] > $_fWidth ? $widths[$field] : $_fWidth);
                    $data[$i][$field] = $_val;
                }

                $i++;
            }
        }

        return $this->setTable($column, $data, $widths);
    }

    public function setTable(array $header, array $data, array $widths) {
        $column = count($header);
        list($pWidth, $pHeight) = $this->_getPageSize($this->curPageSize);
        $contentWidth = $pWidth - $this->rightMargin - $this->leftMargin;
        $cellWidth = floor($contentWidth / $column);
        $_cWhidth = 0;

        $this->setFont($this->conf['fontFamily'], 'B');
        foreach ($header as $hKey => $hVal) {
            $this->cell($widths[$hKey], 6, $hVal, $this->conf['tableBorder'], 0, 'C');
            $_cWhidth += $widths[$hKey];
        }

        $this->ln();
        $this->setFont($this->conf['fontFamily']);

        if (!empty($data)) {
            foreach ($data as $row) {
                foreach ($row as $cKey => $col) {
                    $this->cell($widths[$cKey], 6, $col, $this->conf['tableBorder']);
                }

                $this->ln();
            }
        } else {
            $mpt = ($_cWhidth > $contentWidth ? $contentWidth : $_cWhidth) - ($column - 2);
            $this->cell($mpt, 6, 'Kosong', $this->conf['tableBorder'], 0, 'C');
        }
    }

    public function toFile() {
        $file = $this->docTitle.' - '.$this->docDate.'.pdf';
        $this->output($file, 'F');

        if (file_exists($file)) {
            $dest = ABSPATH.'storage'.DS.$file;
            @rename($file, $dest);

            if ($handle = fopen($dest, 'r')) {
                $dest = str_replace(ABSPATH, '', $dest);
                header('Content-Type: application/x-download');
                header('Content-Disposition: attachment; filename="'.$file.'"');
                header('Cache-Control: private, max-age=0, must-revalidate');
                header('Pragma: public');
                echo fread($handle, filesize($dest));
                fclose($handle);
            }
        }
    }
}
