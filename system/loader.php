<?php defined('ABSPATH') or die ('Not allowed!');

define('DS', DIRECTORY_SEPARATOR);
define('EXT', '.php');
define('SYSPATH', dirname(__FILE__).DS);
define('VENDORPATH', ABSPATH.'vendor'.DS);

define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

/**
 * Memuat Composer autoloader (jika ada)
 */
if (file_exists($composer = VENDORPATH.'autoload'.EXT)) {
    require_once $composer;
}

/**
 * Class Loader
 *
 * Memuat semua file yang ada dalam direktory 'classes'.
 * @link  http://php.net/manual/en/function.spl-autoload-register.php
 */
spl_autoload_register('autoload_register');
function autoload_register($class) {
    $name = strtolower($class);
    $path = SYSPATH.'classes'.DS.$name.EXT;
    if (file_exists($path)) {
        require_once $path;
    }
}

/**
 * Memuat File konfigurasi
 */
$configs = file_exists($confPath = ABSPATH.'configs'.EXT) ? require $confPath : array();

/**
 * Inisialisasi Aplikasi dan menerapkan konfigurasi
 */
$app = new App($configs);

/**
 * Function Loader
 *
 * Memuat semua file yang ada dalam direktory 'functions'.
 */
foreach (glob(SYSPATH.'functions/*'.EXT) as $function) {
    require_once $function;
}

/**
 * Mengaktifkan Mode Debug, ganti 'true' ke 'false' untuk mematikan mode ini.
 * Atau cukup dengan menghapus baris tersebut.
 */
$app->debug(true);

/**
 * Inisialisasi Class Database jika terdapat pengaturan Database dalam file Konfigurasi.
 */
$dbConfigs = isset($configs['db']) ? $configs['db'] : array();
$db = new Db($dbConfigs);

/**
 * Custom error handler
 */
set_error_handler('Error::errHandler');
set_exception_handler('Error::excHandler');

return $app;
