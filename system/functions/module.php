<?php defined('ABSPATH') or die ('Not allowed!');

function module($modName) {
    if (!App::enabledMod($modName)) {
        App::error('Module "'.$modName.'" not found or it\'s disabled.');
    }

    $modClass = ucfirst($modName);
    if (is_subclass_of($modClass, 'Module')) {
        static $mod;
        if ($mod === null) {
            $mod = new $modClass();
        }
        return $mod;
    } else {
        App::error('Module "'.$modName.'" is invalid.');
    }
}
