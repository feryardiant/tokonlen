<?php defined('ABSPATH') or die ('Not allowed!');

/**
 * URL
 * -------------------------------------------------------------------------- */

/**
 * Basis URL aplikasi
 *
 * @param   string  Permalink
 * @return  string
 */
function siteUrl($permalink = '') {
    if (in_array(substr($permalink, 0, 1), array('#', '?'))) {
        $permalink = App::getUri().$permalink;
    }

    return App::conf('baseurl').$permalink ;
}

/**
 * Digunakan untuk pengalihan halaman (URL)
 *
 * @param   string  $url  URL Tujuan
 * @return  void
 */
function redirect($url = '', $delay = false) {
    if (PHP_SAPI != 'cli') {
        $url = strpos('?', $url) === 1 ? currentUrl($url) : siteUrl($url);

        if ($delay !== false) {
            header("refresh: {$delay}; url={$url}");
        } else {
            header("Location: ".$url);
        }

        unset($_POST, $_GET, $_REQUEST);
        exit();
    }
}

/**
 * Digunakan untuk mendapatkan URL saat ini
 *
 * @param   string  $permalink  URL tambahan bila perlu
 * @return  string
 */
function currentUrl($permalink = '', $trim = false) {
    $req = !empty($_GET) ? '?'.http_build_query($_GET) : '';
    $url = siteUrl(App::getUri().$req);

    if ($permalink) {
        $permalink = '/'.$permalink;
    }

    if ($trim === true) {
        $url = rtrim($url, '/');
    }

    return $url.$permalink;
}
