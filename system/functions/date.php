<?php defined('ABSPATH') or die ('Not allowed!');

/**
 * Date Helper
 * -------------------------------------------------------------------------- */

/**
 * Mendapatkan daftar bulan
 *
 * @return  array
 */
function getBulan() {
    $output = array();
    for ( $i = 1; $i <= 12; $i++) {
        $month = date('F', mktime(0, 0, 0, $i, 1));
        $output[$i] = $month;
    }

    return $output;
}

/**
 * Mendapatkan daftar tahun
 *
 * @param   int    $interfal  Selisih tahun
 * @return  array
 */
function getTahun($interfal = 10) {
    $output = array();
    for ( $i = 0; $i <= $interfal; $i++) {
        $year = $i === 0 ? date('Y') : date('Y', mktime(0, 0, 0, $i, 1, date('Y')-$i));
        $output[$year] = $year;
    }

    return $output;
}
