<?php defined('ABSPATH') or die ('Not allowed!');

/**
 * Array Helper
 * -------------------------------------------------------------------------- */

/**
 * Memastikan bahwa $array adalah asosiatif atau tidak
 *
 * @param   array  $array  Array parameter
 * @return  bool
 */
function isArrayAssoc(array $array) {
    $array = array_keys($array);
    $array = array_filter($array, 'is_string');

    return (bool) count($array);
}

/**
 * Menerapkan nilai default pada array
 *
 * @param   array   $array    Array Parameter
 * @param   array   $default  Nilai Default
 * @return  array
 */
function arraySetDefaults(array $array, array $default) {
    foreach ($default as $key => $val) {
        if (!isset($array[$key])) {
            $array[$key] = $val;
        }
    }

    return $array;
}

/**
 * Menerapkan nilai default pada array
 *
 * @param   array   $array    Array Parameter
 * @param   array   $default  Nilai Default
 * @return  array
 */
function arraySetValues(array $array, array $default) {
    foreach ($array as $key => $value) {
        $array[$key] = isset($default[$key]) ? $default[$key] : $value;
    }

    return $array;
}
