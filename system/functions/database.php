<?php defined('ABSPATH') or die ('Not allowed!');

/**
 * Database Helper
 * -------------------------------------------------------------------------- */

/**
 * Menjalankan query
 *
 * @param   string  $string  String yang akan disarung
 * @return  string
 */
function dbQuery($sql, $replacement = '') {
    $db =& Db::instance();
    return $db->query($sql, $replacement);
}

function rowNum($start = 1) {
    $limit = App::conf('db.limit') + $start;
    return ($hal = get('hal')) && $hal != $start ? $limit * $hal - $limit : $start;
}
