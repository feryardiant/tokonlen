--
-- MySQL 5.5.40
-- Tue, 18 Nov 2014 06:56:29 +0000
--

DROP TABLE IF EXISTS `tbl_pengguna`;
CREATE TABLE `tbl_pengguna` (
   `id` int(11) not null auto_increment,
   `username` varchar(50) not null,
   `email` varchar(100) not null,
   `password` varchar(32) not null,
   `level` tinyint(1) not null,
   PRIMARY KEY (`id`),
   UNIQUE KEY (`username`),
   UNIQUE KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_pengguna` (`username`, `email`, `password`, `level`) VALUES
('admin', 'admin@email.com', '81dc9bdb52d04dc20036dbd8313ed055', 1);

-- Update tbl_user structure

ALTER TABLE `tbl_pengguna`
   ADD `fullname` varchar(50) not null AFTER `id`,
   ADD `alamat` VARCHAR(250) NOT NULL AFTER `fullname`;

-- Add fullname to Administrator

UPDATE `tbl_pengguna` SET
   `fullname` = 'Adminis Trator',
   `alamat` = 'Jl. Mana Ajah No 02'
WHERE `tbl_pengguna`.`id` = 1;

-- Add Another pengguna

INSERT INTO `tbl_pengguna` (`fullname`, `alamat`, `username`, `email`, `password`, `level`) VALUES
('Pelanggan', 'Jl. Mana Aja No 01', 'pelanggan', 'pelanggan@email.com', '81dc9bdb52d04dc20036dbd8313ed055', 2);

--

DROP TABLE IF EXISTS `tbl_produk`;
CREATE TABLE `tbl_produk` (
   `id` int(11) not null auto_increment,
   `cat_id` int(11) not null,
   `tgl_post` date NOT NULL,
   `nama` varchar(50) not null,
   `harga` int(11) not null,
   `ket` text not null,
   `stok` int(11) not null,
   `img` varchar(100) not null,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--

INSERT INTO `tbl_produk` (`cat_id`, `tgl_post`, `nama`, `harga`, `ket`, `stok`, `img`) VALUES
(1, '2014-12-23', 'Contoh Produk 1', 10000, 'Deserunt reprehenderit eos, eius assumenda recusandae temporibus aliquid veritatis quo consequatur vitae quod nesciunt, nobis reiciendis, odio? Temporibus delectus error, atque ipsum!', 10, '//placehold.it/200/ddd&text=Produk+1'),
(2, '2014-12-23', 'Contoh Produk 2', 20000, 'Veritatis, expedita neque quae dolor sed rem debitis illum, optio consequatur repellendus mollitia dolores blanditiis velit deleniti similique! Nisi voluptates, blanditiis obcaecati.', 10, '//placehold.it/200/ddd&text=Produk+2'),
(4, '2014-12-23', 'Contoh Produk 3', 30000, 'Commodi ipsa eaque dolorum nisi in quas itaque distinctio, explicabo incidunt eos voluptatibus nostrum, modi quia beatae nam odit, magnam numquam corporis.', 10, '//placehold.it/200/ddd&text=Produk+3'),
(2, '2014-12-23', 'Contoh Produk 4', 20000, 'Voluptas, hic adipisci est dolores praesentium natus! Reiciendis atque, doloribus ab! Ipsa quos voluptatum soluta quibusdam id incidunt velit itaque, laudantium esse!', 10, '//placehold.it/200/ddd&text=Produk+4'),
(3, '2014-12-23', 'Contoh Produk 5', 50000, 'Repellendus natus sequi accusamus accusantium quisquam explicabo autem libero iusto, deserunt quam laborum, quod, alias ipsa. Illum tempora in, autem laborum deleniti!', 10, '//placehold.it/200/ddd&text=Produk+5'),
(1, '2014-12-23', 'Contoh Produk 6', 60000, 'Aliquid enim nesciunt laudantium architecto ut minima quas laborum, ratione at, iusto praesentium id magni doloremque, harum quibusdam nisi eos, distinctio optio.', 10, '//placehold.it/200/ddd&text=Produk+6'),
(2, '2014-12-23', 'Contoh Produk 7', 20000, 'Ipsum odit eaque minima. Esse illum, velit dolorem cumque id totam eaque commodi. Totam laborum numquam et voluptates labore. Id, provident, quae?', 10, '//placehold.it/200/ddd&text=Produk+7'),
(4, '2014-12-23', 'Contoh Produk 8', 80000, 'Magnam voluptate facilis quo quibusdam, temporibus ducimus officiis perspiciatis nihil voluptatem, delectus sequi praesentium harum eaque sed, aliquid labore! Ut, animi, consequuntur!', 10, '//placehold.it/200/ddd&text=Produk+8'),
(3, '2014-12-23', 'Contoh Produk 9', 20000, 'Maxime voluptatum repellendus consectetur amet nihil quibusdam fuga impedit alias, voluptate, corrupti est repellat neque a veniam! Non a, nam accusantium dolorem?', 10, '//placehold.it/200/ddd&text=Produk+9'),
(1, '2014-12-23', 'Contoh Produk 10', 50000, 'Quia qui, amet distinctio officia dignissimos, quaerat cupiditate enim corporis perspiciatis cumque ad. Repudiandae rerum ipsum magni minus esse eos ea alias.', 10, '//placehold.it/200/ddd&text=Produk+10'),
(1, '2014-12-23', 'Contoh Produk 11', 60000, 'Molestias, sit, consequuntur fuga perspiciatis cupiditate labore maiores tempora eius, distinctio voluptas, vero beatae non! Esse pariatur, illo voluptas non vitae, consectetur?', 10, '//placehold.it/200/ddd&text=Produk+11'),
(5, '2014-12-23', 'Contoh Produk 12', 30000, 'Similique dolorem, praesentium ea tempore saepe, expedita tempora laudantium hic quos necessitatibus sint, dignissimos non sed culpa molestiae dolore minima cupiditate impedit.', 10, '//placehold.it/200/ddd&text=Produk+12');

--

DROP TABLE IF EXISTS `tbl_produk_kat`;
CREATE TABLE `tbl_produk_kat` (
   `id` int(11) not null auto_increment,
   `alias` varchar(50) not null,
   `nama` varchar(100) not null,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--

INSERT INTO `tbl_produk_kat` (`alias`, `nama`) VALUES
('kat-1', 'Kategori 1'),
('kat-2', 'Kategori 2'),
('kat-3', 'Kategori 3'),
('kat-4', 'Kategori 4'),
('kat-5', 'Kategori 5');

--

DROP TABLE IF EXISTS `tbl_produk_slide`;
CREATE TABLE IF NOT EXISTS `tbl_produk_slide` (
   `id` int(11) NOT NULL auto_increment,
   `id_produk` int(11) NOT NULL,
   `tgl_post` date NOT NULL,
   `cover` varchar(100) NOT NULL,
   `aktif` tinyint(1) DEFAULT '0',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--

INSERT INTO `tbl_produk_slide` (`id_produk`, `tgl_post`, `cover`, `aktif`) VALUES
(1, '2014-12-21', '//placehold.it/930x300/ddd&text=Produk+Unggulan+1', 1),
(2, '2014-12-22', '//placehold.it/930x300/ddd&text=Produk+Unggulan+2', 1),
(3, '2014-12-23', '//placehold.it/930x300/ddd&text=Produk+Unggulan+3', 1),
(4, '2014-12-24', '//placehold.it/930x300/ddd&text=Produk+Unggulan+4', 0),
(5, '2014-12-25', '//placehold.it/930x300/ddd&text=Produk+Unggulan+5', 0);

--

DROP TABLE IF EXISTS `tbl_belanja`;
CREATE TABLE IF NOT EXISTS `tbl_belanja` (
   `id` int(11) NOT NULL auto_increment,
   `id_pengguna` int(11) NOT NULL,
   `tgl_belanja` date NOT NULL,
   `produk` text NOT NULL,
   `total_harga` int(11) NOT NULL,
   `lunas` tinyint(1) DEFAULT '0',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `tbl_halaman`;
CREATE TABLE `tbl_halaman` (
   `id` int(11) not null auto_increment,
   `alias` varchar(50) not null,
   `judul` varchar(100) not null,
   `tgl_post` date not null,
   `id_penguuna` int(11) not null,
   `isi` text not null,
   PRIMARY KEY (`id`),
   UNIQUE KEY (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
