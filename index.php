<?php

/**
 * Konstanta Aplikasi
 */
define('ROOT',    pathinfo(__FILE__, PATHINFO_BASENAME));
define('ABSPATH', str_replace(ROOT, '', __FILE__));

/**
 * Memuat Sistem
 */
$app = require 'system/loader.php';

/**
 * Memulai Sistem
 */
$app->start();
