<?php defined('ABSPATH') or die('Not allowed!');

return array(
    // URL Aplikasi. (http://localhost/aplikasi)
    'baseurl' => '',
    // Aktifak sesi
    'session'  => true,
    // Template
    'template' => 'tokonlen',
    // Tentang Aplikasi
    'app'      => array(
        // Judul Aplikasi
        'title' => 'Talenta Shop',
        // Keterangan Aplikasi
        'desc' => 'Keterangan Aplikasi',
    ),
    'db' => array(
        // Database Host
        'host' => '',
        // Database Username
        'user' => '',
        // Database Password
        'pass' => '',
        // Database Name
        'name' => '',
        // Database Output limit
        'limit' => 10,
    ),
);
