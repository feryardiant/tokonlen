<?php defined('ABSPATH') or die ('Not allowed!');

template('header'); ?>
<header id="content-header" class="clearfix">
    <h3 id="page-title"><?php echo $heading ?></h3>
</header>
<div id="content-main" class="clearfix">
    <p><?php echo is_array($message) ? implode('</p><p>', $message) : $message ?></p>
</div>
<?php template('footer') ?>
