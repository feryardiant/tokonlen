$(function () {
    // Jquery UI Tab Trigger
    $('.jqui-tabs').hide()
    $(document).ready(function () {
        $('.jqui-tabs').show().tabs()
    })

    // Jquery UI Datepicker Trigger
    $('.jqui-datepicker').datepicker({dateFormat: 'dd-mm-yy'})

    // Form Cancel button function
    $('#cancel-btn').click(function () {
        window.location.href = $('#kembali-btn').attr('href')
    })

    // Data Table Delete button function
    $('.btn-hapus').click(function (e) {
        if (!confirm($(this).data('confirm-text'))) {
            e.preventDefault()
        }
    })

    // Disable click on blank link
    $('a[href="#"]').click(function (e) {
        e.preventDefault();
    })
})
