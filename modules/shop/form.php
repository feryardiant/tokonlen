<?php defined('ABSPATH') or die ('Not allowed!');

if (post('submit')) {
    App::alert('Terjadi kesalahan dalam penyimpanan produk, silahkan periksa kembali.', 'error');
} ?>

<form action="<?php echo currentUrl() ?>" id="user-form" method="post" class="form">
    <div class="control-group">
        <label class="label" for="nama">Nama</label>
        <div class="control-input">
            <input type="text" required name="nama" id="nama">
        </div>
    </div>
    <div class="control-group">
        <label class="label" for="harga">Harga &amp; Stock</label>
        <div class="control-input">
            <input type="text" required name="harga" class="small" id="harga" placeholder="Harga (Rp.)">
            <input type="number" required name="stock" class="small" id="stock" placeholder="Stock">
        </div>
    </div>
    <div class="control-group">
        <label class="label" for="keterangan">Keterangan</label>
        <div class="control-input">
            <textarea required name="keterangan" id="keterangan"></textarea>
        </div>
    </div>
    <div class="control-group">
        <label class="label" for="foto">Foto</label>
        <div class="control-input">
            <input type="file" name="foto">
        </div>
    </div>
    <div class="form control-action">
        <input type="submit" name="submit" id="submit-btn" class="btn" value="Simpan">
        <input type="reset" id="cancel-btn" class="btn" value="Batal">
    </div>
</form>
