<?php defined('ABSPATH') or die ('Not allowed!');
template('header');

$toolbars = array();
if (User::is('admin')) {
    $toolbars['data']['shop?p=form'] = 'Baru';
}

$toolbars['data']['shop/order'] = 'Data Pembelian';
$toolbars['form']['?p=data'] = 'Kembali';

$judul = ucfirst($page);
$file = $page;
?>
<header id="content-header" class="clearfix">
    <h3 id="page-title"><?php echo $judul ?> Produk</h3>
    <?php if (User::loggedin()) echo Menu::toolbar($toolbars[$page]) ?>
</header>
<div id="content-main" class="clearfix">
    <?php include $page.EXT ?>
</div>
<?php template('footer') ?>
