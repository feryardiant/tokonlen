<?php defined('ABSPATH') or die ('Not allowed!');

$sub = App::uriSegment(2);
$term = App::uriSegment(3);
$query = $sub == 'kat' ? Shop::fetch('produk', $term, 'cat_id') : Shop::fetch('produk');
$isHome = App::uriSegment(1) == 'home';
?>
<div id="products" class="fleft">
<?php
if ($sub == 'product' && $term) {
    $row = Shop::fetchOne('produk', $term);
    include 'incl/product-single'.EXT;
} elseif (in_array($sub, array('cart', 'checkout', 'order'))) {
    include 'incl/product-'.$sub.EXT;
} else {
    include 'incl/products'.EXT;
} ?>
</div>
<?php include 'incl/sidebar'.EXT; ?>

