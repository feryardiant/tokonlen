<?php defined('ABSPATH') or die ('Not allowed!');

if (($qslide = Shop::fetchSlide()) && $qslide->count() > 0): ?>
<div id="slider-home"><div class="slider">
<?php foreach ($qslide->result(3) as $row): ?>
    <div class="slide">
        <img src="<?php echo $row->cover ?>" alt="<?php echo $row->nama ?>">
        <?php echo anchor('shop/product/'.$row->id, $row->nama, array('class' => 'slide-text')) ?>
    </div>
<?php endforeach ?>
</div></div>
<?php endif ?>

<script>
$(function () {
    var left = 0,
        width = 930,
        total = width * $('.slide').size()

    $('.slider')
        .css('width', total)
        .css('height', 300)

    setInterval(function () {
        if (left >= -(total - (width * 2))) {
            left = left - width;
        } else {
            left = 0
        }
        $('.slider').css('margin-left', left)
    }, 5000);
})
</script>

<?php include 'data'.EXT; ?>
