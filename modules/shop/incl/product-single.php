<?php defined('ABSPATH') or die ('Not allowed!') ?>

<div class="product-single">
    <img src="<?php echo $row->img ?>" alt="<?php echo $row->nama ?>">
    <div class="detail">
        <h3 class="title"><?php echo $row->nama ?></h3>
        <div class="meta">
            <?php echo anchor('shop/cart/'.$row->id.'?do=add', 'Beli', array('class' => 'btn')) ?>
            <span>Rp. <?php echo formatAngka($row->harga) ?></span>
        </div>
        <p><?php echo $row->ket ?></p>
    </div>
</div>
