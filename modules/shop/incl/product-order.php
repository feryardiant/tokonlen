<?php defined('ABSPATH') or die ('Not allowed!');
$isAdmin = User::is('admin'); ?>

<div class="product-cart">
<table class="data">
    <thead>
        <tr>
            <th style="width:30%;"><?php echo $isAdmin ? 'Nama Pelanggan' : 'Order' ?></th>
            <th style="width:15%;">Tanggal</th>
            <th style="width:20%;">Total (Rp.)</th>
            <th style="width:20%;">Status</th>
            <th class="action">Pilihan</th>
        </tr>
    </thead>
    <tbody>
    <?php if (($query = Shop::order()) && ($total = $query->count()) > 0) : foreach ($query->result(true) as $row) : ?>
        <tr id="order-<?php echo $row->id ?>">
            <td><?php echo $isAdmin ? anchor($row->id_pengguna, $row->fullname) : formatTanggal($row->tgl_belanja, 'dmy').'-'.$row->id ?></td>
            <td class="acenter"><?php echo formatTanggal($row->tgl_belanja) ?></td>
            <td class="aright"><?php echo formatAngka($row->total_harga) ?></td>
            <td class="acenter"><?php echo $row->lunas == 1 ? 'Lunas' : 'Belum dibayar' ?></td>
            <td class="action"><div class="btn-group">
            <?php if ($isAdmin): ?>
                <?php echo anchor('?p=form&id='.$row->id, 'Ubah', array('class' => 'btn btn-edit')) ?>
                <?php echo anchor('?p=hapus&id='.$row->id, 'Hapus', array('class' => 'btn btn-hapus')) ?>
            <?php else: ?>
                <?php echo $row->lunas == 0 ? anchor('?p=form&id='.$row->id, 'Bayar', array('class' => 'btn btn-edit')) : '' ?>
            <?php endif ?>
            </div></td>
        </tr>
    <?php endforeach; else: ?>
        <tr><td colspan="4" style="text-align:center;">Belum ada data.</td></tr>
    <?php endif; ?>
    </tbody>
</table>
</div>
