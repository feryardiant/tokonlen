<?php defined('ABSPATH') or die ('Not allowed!') ?>

<div class="product-cart">
<?php if (Shop::checkout()): ?>
    <span class="alert success">Terima kasih telah berbelanja di <?php echo App::conf('app.title') ?>. Segeralah melakukan pembayaran agar pesanan anda dapat secepatnya kami proses.</span>
<?php else: ?>
    <span class="alert error">Oops! Kesalahan.</span>
<?php endif ?>
</div>
