<?php defined('ABSPATH') or die ('Not allowed!');

if ($query && ($total = $query->count()) > 0): $c = 1; foreach ($query->result(true) as $row): ?>
    <div class="product">
        <img src="<?php echo $row->img ?>" width="150" height="150" alt="<?php echo $row->nama ?>">
        <?php echo anchor('shop/product/'.$row->id, $row->nama) ?>
        <span>Rp. <?php echo formatAngka($row->harga) ?></span>
    </div>
<?php
    if ($c % 4 == 0) echo '<hr>';
    if ($isHome && $c == 8) break;
    $c++; endforeach; else: ?>
    <span class="alert warning no-product">Tidak ada produk.</span>
<?php endif;
if ($isHome && $total) echo anchor('shop', 'Lihat selengkapnya', array('class' => 'btn')); ?>
