<?php defined('ABSPATH') or die ('Not allowed!');
$qside = Shop::fetchCat(); ?>

<div id="product-sidebar" class="sidebar">
    <div class="widget">
        <h4 class="widget-title">Pencarian</h4>
        <form action="<?php echo currentUrl() ?>" id="product-search">
            <input type="search" name="search" id="search">
            <input type="submit" name="s" id="s-btn" class="btn" value="Cari">
        </form>
    </div>
    <div class="widget">
        <h4 class="widget-title">Kategori <?php if (User::is('admin')) echo anchor('shop/kat?p=form', 'Buat kategori', array('class' => 'btn fright')) ?></h4>
        <ul class="widget-content">
        <?php if ($qside && $qside->count() > 0) : foreach ($qside->result() as $row) : ?>
            <li><?php echo anchor('shop/kat/'.$row->id, $row->nama) ?></li>
        <?php endforeach; else: ?>
            <li>Kategori kosong.</li>
        <?php endif; ?>
        </ul>
    </div>
</div>
