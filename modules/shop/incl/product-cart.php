<?php defined('ABSPATH') or die ('Not allowed!');

if ($id = App::uriSegment(3)):
    Shop::cart($id, get('do'));
else: ?>
    <div class="product-cart">
    <?php if ($cartItems = Shop::cartItems()): $total = 0; ?>
        <h4 class="cart-title">Yay! Anda telah memilih <?php echo count($cartItems) ?> produk. <?php echo anchor('shop/', 'Mau nambah?', array('class' => 'btn')) ?></h4>
        <?php if ($query && $query->count() > 0): foreach ($query->result() as $row): if (in_array($row->id, array_keys($cartItems))): ?>
        <div class="item clearfix">
            <img src="<?php echo $row->img ?>" alt="<?php echo $row->nama ?>">
            <div class="detail">
                <h4><?php echo anchor('shop/product/'.$row->id, $row->nama) ?></h4>
                <span><?php echo $cartItems[$row->id].' &times; @ Rp. '.formatAngka($row->harga) ?></span>
                <?php $subtotal = $cartItems[$row->id] * $row->harga; $total += $subtotal; ?>
                <span class="fright bold"><?php echo 'Subtotal: Rp. '.formatAngka($subtotal) ?></span>
                <nav class="page-toolbar cart-action">
                    <?php echo anchor('shop/cart/'.$row->id.'?do=remove', 'Hapus', array('class' => 'btn toolbar-btn')) ?>
                    <nav class="btn-group toolbar-btn">
                        <?php echo anchor('shop/cart/'.$row->id.'?do=reduce', 'Kurangi', array('class' => 'btn toolbar-btn')) ?>
                        <?php echo anchor('shop/cart/'.$row->id.'?do=add', 'Tambah', array('class' => 'btn toolbar-btn')) ?>
                    </nav>
                </nav>
            </div>
        </div>
        <?php endif; endforeach; endif; ?>
        <form action="<?php echo siteUrl('shop/checkout/') ?>" method="post">
            <input type="hidden" name="total-harga" value="<?php echo $total ?>">
            <input type="submit" name="checkout" value="Proses Sekarang" class="btn">
            <h4 class="total-text"><?php echo 'Total: Rp. '.formatAngka($total) ?></h4>
        </form>
    <?php else: ?>
        <div class="item clearfix">
            <span class="alert warning">Oops! Troli anda kosong,<br>silahkan pilih produk yang anda suka dilanjutkan dengan menekan tombol "beli"</span>
            <?php echo anchor('shop/product/', 'Lanjutkan belanja', array('class' => 'btn')) ?>
        </div>
    <?php endif ?>
    </div>
<?php endif ?>
