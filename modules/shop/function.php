<?php defined('ABSPATH') or die ('Not allowed!');

class Shop extends Module
{
    private static
        $tAlias = array(
            'belanja' => 'tbl_belanja',
            'produk' => 'tbl_produk',
            'slide' => 'tbl_produk_slide',
            'kat' => 'tbl_produk_kat',
        );

    public function __construct() {
        $module = get_class($this);
        $this->initialize(strtolower($module), array(
            'title' => 'Toko',
            'order' => 2,
        ));
    }

    public static function cartItems() {
        if ($items = App::session('cart_items')) {
            $items = unserialize($items);
            return $items;
        }

        return array();
    }

    public static function cart($id, $action) {
        if (!($items = self::cartItems())) {
            App::session('cart_items', '');
            $items = array();
        }

        switch ($action) {
            case 'add':
                $items[$id] = isset($items[$id]) ? $items[$id] + 1 : 1;
                break;

            case 'reduce':
                if (isset($items[$id])) {
                    if ($items[$id] > 1) {
                        $items[$id] = $items[$id] - 1;
                    } else {
                        unset($items[$id]);
                    }
                }
                break;

            case 'remove':
                if (isset($items[$id])) {
                    unset($items[$id]);
                }
                break;
        }

        App::session('cart_items', serialize($items));
        redirect('shop/cart');
    }

    public static function checkout() {
        if (!User::loggedin()) {
            redirect('user?p=form&do=login&redir=shop/cart');
        }

        if ($items = self::cartItems()) {
            $belanja = array(
                'id_pengguna' => User::current('id'),
                'tgl_belanja' => date('Y-m-d'),
                'produk'      => serialize($items),
                'total_harga' => post('total-harga'),
            );

            if ($return = static::$db->save(self::$tAlias['belanja'], $belanja)) {
                App::session('cart_items', '');
                return $return;
            }
        }

        return false;
    }

    public static function order() {
        $lunas = (int) get('lunas') ?: 0;
        $sql = 'SELECT b.*, u.fullname FROM %s b INNER JOIN %s u ON u.id=b.id_pengguna WHERE b.lunas='.$lunas;

        if (!User::is('admin')) {
            $sql .= ' AND b.id_pengguna='.User::current('id');
        }

        return static::$db->query($sql, array(self::$tAlias['belanja'], 'tbl_pengguna'));
    }

    public static function fetch($table, $val = false, $key = '') {
        $where = array();
        if ($val !== false) {
            $key || $key = 'id';
            $where = array($key => $val);
        }

        return static::$db->select(self::$tAlias[$table], '', $where);
    }

    public static function fetchOne($table, $val = false, $key = '') {
        if ($query = self::fetch($table, $val, $key)) {
            return $query->fetchOne();
        }

        return false;
    }

    public static function save() {
        if ($katId = post('kategori')) {
            $kat = static::fetchOneCat($katId, 'id');
        }

        $formData = array(
            'nama'  => post('nama'),
            'harga' => (int) post('harga'),
            'ket'   => (int) post('ket'),
            'stok'  => (int) post('stok'),
            'img'   => '350',
        );
    }

    public static function delete() {}

    public static function fetchCat($val = false, $key = '') {
        return self::fetch('kat', $val, $key);
    }

    public static function fetchOneCat($val = false, $key = '') {
        return self::fetchOne('kat', $val, $key);
    }

    public static function saveCat() {}

    public static function deleteCat() {}

    public static function fetchSlide($val = false, $key = '') {
        $sql = 'SELECT p.*, s.cover, s.aktif FROM %s p INNER JOIN %s s ON s.id_produk=p.id WHERE s.aktif=1';
        return static::$db->query($sql, array(self::$tAlias['produk'], self::$tAlias['slide']));
    }

    public static function fetchOneSlide($val = false, $key = '') {
        return self::fetchOne('slide', $val, $key);
    }

    public static function saveSlide() {}

    public static function deleteSlide() {}
}
