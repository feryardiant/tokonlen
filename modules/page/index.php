<?php defined('ABSPATH') or die ('Not allowed!');
template('header');
$toolbars = array();
if (User::is('admin')) {
    $toolbars['data']['shop?p=form'] = 'Baru';
} ?>
<header id="content-header" class="clearfix">
    <h3 id="page-title">Halaman</h3>
    <?php if (User::is('admin')) echo Menu::toolbar($toolbars[$page]) ?>
</header>
<div id="content-main" class="clearfix">
    <div id="products" class="fleft"><?php include $page.EXT ?></div>
</div>
<?php template('footer') ?>
