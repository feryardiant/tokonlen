<?php defined('ABSPATH') or die ('Not allowed!');

$query = Page::fetch(); ?>

<?php if ($query && ($total = $query->count()) > 0) : foreach ($query->result(true) as $row) : ?>
    <div class="product">
        <?php echo anchor('page/'.$row->alias, $row->judul) ?>
        <p><?php echo $row->isi ?></p>
    </div>
<?php endforeach; else: ?>
    <span class="alert warning no-product">Tidak ada produk.</span>
<?php endif; ?>
</div>

<?php if ($query && $total): ?>
<div class="data-info clearfix">
    <p class="data-total">Total data: <?php echo $total ?></p>
    <div class="data-page"><?php echo pagination($total) ?></div>
</div>
<?php endif ?>
