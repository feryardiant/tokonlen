<?php defined('ABSPATH') or die ('Not allowed!');

class page extends Module
{
    public function __construct() {
        $module = get_class($this);
        $this->initialize(strtolower($module), array(
            'title' => 'Halaman',
        ));
    }

    public static function fetch($val = false, $key = '') {
        $where = array();
        if ($val !== false) {
            $key || $key = 'id';
            $where = array($key => $val);
        }

        return static::$db->select('tbl_halaman', '', $where);
    }
}
