<?php defined('ABSPATH') or die ('Not allowed!');

class User extends Module
{
    public static
        $levels  = array(1 => 'Administrator', 2 => 'Pelanggan'),
        $aliases = array('admin' => 1, 'pelanggan' => 2);

    /**
     * Class Constructor
     *
     * @param  array  $configs  Konfigurasi
     */
    public function __construct() {
        $module = get_class($this);
        $this->initialize(strtolower($module), array(
            'caps' => array(1),
            'title' => 'Pengguna',
            'order' => 0,
        ));

        $do = get('user');
        if ($do == 'logout') {
            App::dropSession();
            redirect();
        } elseif (in_array($do, array('login', 'register'))) {
            if (static::loggedin()) redirect();
            redirect('user?p=form&do='.$do);
        }
    }

    public static function loginPage() {
        return dirname(__FILE__).DS.'loginform'.EXT;
    }

    public static function fetch($val = false, $key = '') {
        $where = array();
        if ($val !== false) {
            $key || $key = 'id';
            $where = array($key => $val);
        }

        return static::$db->select('tbl_pengguna', '', $where);
    }

    public static function data($val = false, $key = '') {
        if ($query = self::fetch($val, $key)) {
            return $query->fetchOne();
        }

        return false;
    }

    public static function loggedin() {
        return App::session('auth') !== false;
    }

    public static function is($alias) {
        if (!isset(self::$aliases[$alias])) return false;
        return (int) self::$aliases[$alias] == (int) App::session('level');
    }

    public static function current($key) {
        return App::session($key);
    }

    public static function menu() {
        $menu = '';
        $loggedin = User::loggedin();
        if (!$loggedin || !self::is('admin')) {
            $menu = anchor('shop/cart', 'Troli <span class="product-count">'.count(Shop::cartItems()).'</span>', array('class' => 'cart-btn'));
        }

        if ($loggedin) {
            $menu .= '<strong>Hallo, '.User::current('username').'</strong> '
                  .  anchor('?user=logout', 'Logout');
        } else {
            $menu .= anchor('?user=login', 'Akun Saya');
        }

        return $menu;
    }

    public static function login() {
        $query = static::$db->select('tbl_pengguna', '', array(
            'username' => post('username')
        ));

        if ($query && ($logindata = $query->fetchOne()) && $logindata->password == md5(post('password'))) {
            App::session(array(
                'auth'     => 1,
                'id'       => $logindata->id,
                'username' => $logindata->username,
                'level'    => $logindata->level,
            ));

            return true;
        }

        return false;
    }

    public static function getAlias($level) {
        return array_search($level, self::$aliases);
    }

    public static function simpan() {
        if (($user = self::fetch($warga->nik, 'nik')->fetchOne()) || $user->username == post('username')) {
            App::error('Pengguna dengan data tersebut sudah ada.');
        }

        $data = array(
            'username' => post('username'),
            'email'    => post('email'),
            'level'    => (int) post('level'),
        );

        if ($pass = post('password')) {
            $data['password'] = md5($pass);
        }

        $term = ($id = get('id')) ? array('id' => $id) : array();

        return static::$db->save('tbl_pengguna', $data, $term);
    }
}
