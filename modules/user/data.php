<?php defined('ABSPATH') or die ('Not allowed!');

if ($level = get('level')) {
    $query = User::fetch($level, 'level');
} else {
    $query = User::fetch();
} ?>
<table class="data">
    <thead>
        <tr>
            <th style="width:35%;">Username</th>
            <th style="width:35%;">Email</th>
            <th style="width:20%;">Level</th>
            <th class="action">Pilihan</th>
        </tr>
    </thead>
    <tbody>
    <?php if ($query && ($total = $query->count()) > 0) : foreach ($query->result(true) as $row) : ?>
        <tr>
            <td><?php echo $row->username ?></td>
            <td><?php echo $row->email ?></td>
            <td><?php echo User::$levels[$row->level] ?></td>
            <td class="action">
                <div class="btn-group">
                <?php echo anchor('?p=form&id='.$row->id, 'Ubah', array('class' => 'btn btn-edit')) ?>
                <?php echo anchor('?p=hapus&id='.$row->id, 'Hapus', array('class' => 'btn btn-hapus')) ?>
                </div>
            </td>
        </tr>
    <?php endforeach; else: ?>
        <tr><td colspan="4" style="text-align:center;">Belum ada data.</td></tr>
    <?php endif; ?>
    </tbody>
</table>

<?php if ($query): ?>
<div class="data-info clearfix">
    <p class="data-total">Total data: <?php echo $total ?></p>
    <div class="data-page"><?php echo pagination($total) ?></div>
</div>
<?php endif ?>
