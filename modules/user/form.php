<?php defined('ABSPATH') or die ('Not allowed!');
$do = get('do'); ?>

<form action="<?php echo currentUrl() ?>" id="user-form" method="post" class="form">
    <input type="hidden" name="action" value="<?php echo $do ?>">
    <div class="control-group">
        <label class="label" for="username">Username</label>
        <div class="control-input">
            <input type="text" required name="username" id="username">
        </div>
    </div>
<?php if ($do == 'register'): ?>
    <input type="hidden" name="level" value="0">
    <div class="control-group">
        <label class="label" for="nama">Nama Lengkap</label>
        <div class="control-input">
            <input type="text" required name="nama" id="nama">
        </div>
    </div>
    <div class="control-group">
        <label class="label" for="email">Email</label>
        <div class="control-input">
            <input type="email" required name="email" id="email">
        </div>
    </div>
<?php endif ?>
    <div class="control-group">
        <label class="label" for="password">Password</label>
        <div class="control-input">
            <input type="password" required name="password" id="password">
        </div>
    </div>
<?php if ($do == 'register'): ?>
    <div class="control-group">
        <label class="label" for="passconf">Ulangi Password</label>
        <div class="control-input">
            <input type="password" required name="pass_conf" id="pass_conf">
        </div>
    </div>
<?php endif ?>
<?php if (User::is('admin')): ?>
    <div class="control-group">
        <label class="label" for="level">Level</label>
        <div class="control-input">
            <select required name="level" id="level">
                <option>---</option>
                <?php foreach (User::$levels as $val => $level) : ?>
                    <option <?php echo ($id and $data->level == $val) ? 'selected' : '' ?> value="<?php echo $val ?>"><?php echo $level ?></option>
                <?php endforeach ?>
            </select>
        </div>
    </div>
<?php endif ?>
    <div class="form control-action">
    <?php if (!User::loggedin()): ?>
        <?php if ($do == 'register'): ?>
            <input type="submit" name="register" id="submit-btn" class="btn" value="Kirim" autofocus>
            <?php echo anchor('?user=login', 'Login', array('class' => 'btn fright')) ?>
        <?php else: ?>
            <input type="submit" name="login" id="submit-btn" class="btn" value="Login">
            <?php echo anchor('?user=register', 'Registrasi', array('class' => 'btn fright')) ?>
        <?php endif ?>
    <?php else: ?>
        <input type="submit" name="submit" id="submit-btn" class="btn" value="Simpan">
        <input type="reset" id="cancel-btn" class="btn" value="Batal">
    <?php endif ?>
    </div>

</form>
