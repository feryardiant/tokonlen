--
-- MySQL 5.5.40
-- Tue, 18 Nov 2014 06:56:29 +0000
--

DROP TABLE IF EXISTS `tbl_pengguna`;
CREATE TABLE `tbl_pengguna` (
   `id` int(11) not null auto_increment,
   `username` varchar(50) not null,
   `email` varchar(100) not null,
   `password` varchar(32) not null,
   `level` tinyint(1) not null,
   PRIMARY KEY (`id`),
   UNIQUE KEY (`username`),
   UNIQUE KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_pengguna` (`username`, `email`, `password`, `level`) VALUES
('admin', 'admin@email.com', '81dc9bdb52d04dc20036dbd8313ed055', 1);
