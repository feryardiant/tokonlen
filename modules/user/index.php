<?php defined('ABSPATH') or die ('Not allowed!');

template('header');

if ($action = post('action')) {
    $return = false;
    $message = '';
    if ($action == 'login') {
        $return = User::login();
    } elseif ($action == 'register') {
        try {
            $return = User::simpan();
        } catch (Exception $e) {
            $message = $e->getMessage();
        }
    }

    if ($return) {
        App::alert(ucfirst($action).' berhasil', 'success');
        $redir = get('redir') ?: '';
        if ($action == 'register') redirect($redir, 4);
        redirect($redir);
    } else {
        App::alert(ucfirst($action).' gagal. '.$message, 'error');
    }
}

if ($id = get('id')) {
    $data = User::data($id, 'id');
}

$toolbars = array();
if (User::is('admin')) {
    $toolbars['data']['?p=form'] = 'Baru';
}
$toolbars['data'][1]['?p=data'] = 'Semua';
$toolbars['form']['?p=data'] = 'Semua';
foreach (User::$levels as $key => $val) {
    $toolbars['data'][1]['?p=data&level='.$key] = $val;
}

$judul = ucfirst($page);
$file = $page;
?>

<header id="content-header" class="clearfix">
    <h3 id="page-title"><?php echo $judul ?> Pengguna</h3>
    <?php if (User::loggedin() && isset($toolbars[$page])) echo Menu::toolbar($toolbars[$page]) ?>
</header>
<div id="content-main" class="clearfix">
    <?php include $file.'.php' ?>
</div>

<?php template('footer') ?>
